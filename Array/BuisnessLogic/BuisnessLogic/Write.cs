﻿using System;
using System.Text;
using Interfaces.Interfaces;
using System.IO;

namespace BuisnessLogic
{
    public class Write : IWrite
    {
        public void WriteArray(string path, int[] array)
        {
            if (path != null)
            {
                File.WriteAllLines(path, Array.ConvertAll(array, Convert.ToString), Encoding.Default);
            }
        }
    }
}
