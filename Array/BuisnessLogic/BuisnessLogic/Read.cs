﻿using System;
using System.Text;
using System.IO;
using Interfaces.Interfaces;

namespace BuisnessLogic
{
    public class Read : IRead 
    {
        public int[] ReadArray(string path)
        {
            if (!File.Exists(path))
            {
                Console.WriteLine("Этого файла {0} не существует!", path);
                return null;
            }
            if (new FileInfo(path).Length != 0L)
                return Array.ConvertAll(File.ReadAllLines(path, Encoding.Default), int.Parse);
            Console.WriteLine("Файл {0} пустой!", path);
            return null;
        }
    }
}
