﻿using System;
using Interfaces.Interfaces;
using BuisnessLogic;

namespace Arrays_Task1
{
    class Manager
    {
        private int numberOfSort;
        private string input = null;
        private string output = null;
        private int[] text;
        private IRead read;
        private IWrite write;
        private bool error = false;
        private ISort sort;



        private void Parsing(string[] args)
        {
            for (int index = 0; index < args.Length; ++index)
            {

                string s = args[index];
                if (s == "-w")
                {
                    if (index + 1 >= args.Length) error = true;
                    output = args[index + 1];

                }

                else
                {
                    if (s == "-r")
                    {
                        if (index + 1 >= args.Length) error = true;
                        input = args[index + 1];

                    }

                    else
                    {

                        if (s == "/q")
                        {
                            if (numberOfSort != 0)
                                error = true;
                            numberOfSort = 1;
                        }

                        else
                        {

                            if (s == "/s")
                            {
                                if (numberOfSort != 0) error = true;
                                numberOfSort = 2;
                            }

                            else
                            {


                                if (s == "/i")
                                {
                                    if (numberOfSort != 0) error = true;
                                    numberOfSort = 3;
                                }

                                else
                                {


                                    if (s == "/b")
                                    {
                                        if (numberOfSort != 0) error = true;
                                        numberOfSort = 4;
                                    }

                                    else
                                    {
                                        if (s == "/c")
                                        {
                                            if (numberOfSort != 0) error = true;
                                            numberOfSort = 5;
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
            }
        }



        private void Run()
        {
            text = read.ReadArray(input);
            if (text == null)
                return;
            int[] array;
            switch (numberOfSort)
            {
                case 1:
                    array = sort.QuickSorting(text);
                    write.WriteArray(output, array);
                    break;
                case 2:
                    array = sort.SelectionSorting(text);
                    write.WriteArray(output, array);
                    break;
                case 3:
                    array = sort.InsertionSorting(text);
                    write.WriteArray(output, array);
                    break;
                case 4:
                    array = sort.BubbleSorting(text);
                    write.WriteArray(output, array);
                    break;
                case 5:
                    array = sort.CocktailSorting(text);
                    write.WriteArray(output, array);
                    break;
            }
        }

        public Manager(string[] args)
        {
            Parsing(args);
            if (!error)
            {
                read = new Read();
                sort = new Sort();
                write = new Write();
                Run();
            }
            else Console.WriteLine("Вы не правильно ввели аргументы !");
        }
    }
}
