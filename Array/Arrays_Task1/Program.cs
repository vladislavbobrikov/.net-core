﻿using System;

namespace Arrays_Task1
{
    class Program
    {
        static void Main(string[] args)
        {
            if (args.Length == 0) Help();
            else
            {
                if (args[0] == "/?") Help();
                else { Manager manager = new Manager(args); }
            }

        }

        static void Help()
        {
            Console.WriteLine("Справка:");
            Console.WriteLine();
            Console.WriteLine("Пример запуска: Arrays_Task1.exe /b -r inputarray.txt -w sortedarray.txt ");
            Console.WriteLine();
            Console.WriteLine("Ключи: -r (название файла) - вхоядщий файл, -w (название файла)- исходящий файл, /? - справка");
            Console.WriteLine();
            Console.WriteLine("Алгоритмы сортировки массива: ");
            Console.WriteLine("/b - пузырьком");
            Console.WriteLine("/q - быстрая сортировка");
            Console.WriteLine("/i - сортировка вставками");
            Console.WriteLine("/s - сортировка выборкой");
            Console.WriteLine("/c - котейльная сортировка");


        }
    }
}
