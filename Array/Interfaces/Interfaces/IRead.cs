﻿namespace Interfaces.Interfaces
{
    public interface IRead
    {
        int[] ReadArray(string path);
    }
}
