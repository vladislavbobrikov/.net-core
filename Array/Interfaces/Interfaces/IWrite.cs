﻿namespace Interfaces.Interfaces
{
    public interface IWrite
    {
        void WriteArray(string path, int[] array);
    }
}
