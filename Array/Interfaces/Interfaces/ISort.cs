﻿namespace Interfaces.Interfaces
{
    public interface ISort
    {
        int[] QuickSorting(int[] array);
        int[] BubbleSorting(int[] array);
        int[] SelectionSorting(int[] array);
        int[] InsertionSorting(int[] array);
        int[] CocktailSorting(int[] array);
    }
}
