﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Buisness_Logic
{
    public class Rectangle : Figure
    {
        double a;
        double b;

        public Rectangle(double a, double b) : base(a,b)
        {
            this.a = a;
            this.b = b;
        }

        public override double Perimeter()
        {
            return a * 2 + b * 2;
        }

        public override double Area()
        {
            return a * b;
        }

    }
}
