﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Buisness_Logic
{
    public abstract class Figure
    {
        double a;
        double b;
        double c;

        public Figure(double a, double b, double c)
        {
            this.a = a;
            this.b = b;
            this.c = c;
        }

        public Figure(double a, double b)
        {
            this.a = a;
            this.b = b;
        }

        public Figure(double a)
        {
            this.a = a;
        }

        public abstract double Perimeter();
        public abstract double Area();

    }
}
