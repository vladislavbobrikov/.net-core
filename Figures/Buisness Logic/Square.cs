﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Buisness_Logic
{
    public class Square : Figure
    {
        double a;

        public Square(double a) : base(a)
        {
            this.a = a;
        }

        public override double Perimeter()
        {
            return a * 4;
        }

        public override double Area()
        {
            return a * a;
        }

    }
}
