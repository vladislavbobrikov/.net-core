﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Buisness_Logic
{
    public class Triangle : Figure
    {
        double a;
        double b;
        double c;

        public Triangle(double a, double b, double c) : base(a,b,c)
        {
            this.a = a;
            this.b = b;
            this.c = c;
        }

        public override double Perimeter()
        {
            return a + b + c;
        }

        public override double Area()
        {
            double p = (a + c + b) / 2;
            return Math.Sqrt(p * (p - a) * (p - b) * (p - b));
        }

    }
}
