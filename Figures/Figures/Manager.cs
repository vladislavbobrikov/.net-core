﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Buisness_Logic;

namespace Figures
{
    class Manager
    {
        public Manager()
        {
            k:
            Console.WriteLine("Выбирите фигуру, площадь и перимиетр которой вы хотите найти:");
            Console.WriteLine("/sq - квадрат.");
            Console.WriteLine("/rg - прямоугольник.");
            Console.WriteLine("/tq - квадрат.");
            Console.WriteLine("");
            Console.WriteLine("/exit - закрыть программу.");
            string index = Console.ReadLine();
            switch (index)
            {
                case "/sq":
                    Console.Write("Введите сторону квадрарта: ");
                    double side = Convert.ToDouble(Console.ReadLine());
                    Console.WriteLine("Периметр равен:");
                    Square sqr = new Square(side);
                    Console.WriteLine(Convert.ToString(sqr.Perimeter()));
                    Console.WriteLine("Площадь равна:");
                    Console.WriteLine(Convert.ToString(sqr.Area()));
                    goto k;
                case "/rg":
                    Console.WriteLine("Введите стороны прямоугольника:");
                    double firstSide = Convert.ToDouble(Console.ReadLine());
                    double secondSide = Convert.ToDouble(Console.ReadLine());
                    Rectangle rctg = new Rectangle(firstSide, secondSide);
                    Console.WriteLine("Периметр равен");
                    Console.WriteLine(Convert.ToString(rctg.Perimeter()));
                    Console.WriteLine("Площадь равна:");
                    Console.WriteLine(Convert.ToString(rctg.Area()));
                    goto k;
                case "/tg":
                    Console.WriteLine("Введите стороны треугольника:");
                    double _firstSide = Convert.ToDouble(Console.ReadLine());
                    double _secondSide = Convert.ToDouble(Console.ReadLine());
                    double _thirdSide = Convert.ToDouble(Console.ReadLine());
                    Triangle trg = new Triangle(_firstSide, _secondSide, _thirdSide);
                    Console.WriteLine("Периметр равен");
                    Console.WriteLine(Convert.ToString(trg.Perimeter()));
                    Console.WriteLine("Площадь равна:");
                    Console.WriteLine(Convert.ToString(trg.Area()));
                    goto k;
                case "/exit":
                    Environment.Exit(0);
                    break;
            }
            Console.ReadKey();
        }
    }
}
