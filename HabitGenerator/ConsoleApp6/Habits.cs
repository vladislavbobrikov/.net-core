﻿using System;


namespace HabitGenerator
{
    [Serializable]
    public class Habits
    {
        public string _habit { get; set; }

        public Habits() { }

        public Habits(string habit)
        {
            _habit = habit;
        }
    }
}
