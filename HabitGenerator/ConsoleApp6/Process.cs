﻿using System;
using System.Xml.Serialization;
using System.IO;

namespace HabitGenerator
{
    class Process
    {
        string habit;
        static string[] habits = new string[]
        {
            "Воздержание. Есть не до пресыщения, пить не до опьянения.",
            "Молчание. Говорить только то, что может принести пользу мне или другому; избегать пустых разговоров.",
            "Порядок. Держать все свои вещи на их местах; для каждого занятие иметь свое время.",
            "Решительность. Решаться выполнить то, что должно сделать; неискуснительно выполнять то, что решено.",
            "Брежливость. Тратить деньги только на то, что приносит благо мне или другим, то есть ничего не расточать.",
            "Трудолюбие. Не терять времени попусту; быть всегда занятыми чем-либо полезным; отказываться от всех не нужных действий.",
            "Искренность. Не причинять вредного обмана, иметь чистые и справедливые мысли; в разговоре также придерживаться этого правила.",
            "Справедливость. Не причинять никому вреда; не совершать несправедливостей и не опускать добрых дел, которые входят в число твоих обязанностей",
            "Умеренность. Избегать крайностей; сдерживать, насколько ты считаешь это уместным, чувство обиды о несправедливостей.",
            "Чистота. Не допускать телесной нечистоты; соблюдать опрятность в одежде и в жилище.",
            "Спокойствие. Не волноваться по пустякам и по поводу обычных и неизбежных случаев."
        };
        public string aHabit()
        {

            Random radn = new Random();
            for (int i = 0; i < habits.Length; i++)
            {
                habit = habits[radn.Next(0, habits.Length)];
                break;
            }
            return habit;
        }

        public Process()
        {
            Habits hab = new Habits(aHabit());
            Console.WriteLine("Генератор полезных привычек: ");
            Console.WriteLine("Чтобы записать в файл привычку введите /s, чтобы прочитать из файла введите /r");
            XmlSerializer formatter = new XmlSerializer(typeof(Habits));
            string command = Console.ReadLine();
            switch(command)
            {
                case "/s":
                    using (FileStream fs = new FileStream("habits.xml", FileMode.Create))
                    {
                        formatter.Serialize(fs, hab);
                    }
                    break;
                case "/r":
                    using (FileStream fs = new FileStream("habits.xml", FileMode.OpenOrCreate))
                    {
                        Habits newHabit = (Habits)formatter.Deserialize(fs);
                        Console.WriteLine("Объект десериализован");
                        Console.WriteLine(newHabit._habit);
                        Console.ReadKey();
                    }
                    break;
            }
        }
    }
}
